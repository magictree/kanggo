## Table of Contents
- [Contributing](#contributing)
- [The Standards](#the-standards)
  * [Commit Message](#commit-message)
      - [Easy to Understand Message](#easy-to-understand-message)
      - [Conventional Commit Standard](#conventional-commit-standard)
  * [Git Workflow](#git-workflow)
      - [Adding Changes](#adding-changes)
      - [Create an Issue Ticket](#create-an-issue-ticket)
      - [Creating Pull Requests](#creating-pull-requests)
- [Disclaimer](#disclaimer)

# Contributing

Although we accept all kind of contribution in this project, we do have a standard for what, and how to contribute on this project. We expect everyone who contribute on this project to follow the standard.

## The Standards

### Commit Message

#### Easy to Understand Message

All commit message should be easily understood just by reading the message. If you need to check the code to see what does the commit do, you failed. 

Structurally, your commit message should be 75 character (or less) summary of what will this commit do. You know your git commit message doing well if your message can fit nicely in this sentence `If applied, this commit will <insert commit message here>.`

If you need to add more details to the commit message, do it on the commit body. The description can be added by adding new paragraph below the commit summary, separated by a new line. Your description should describe what does your commit do in more detail. Don't just repeat the summary, but expand on it. Add all of the details missing from the summary which is important, but not crucial to see what does the commit do at a glance. If you need those crucial details, add it on the commit summary.

For more information for the git commit message guideline: [https://gist.github.com/robertpainsi/b632364184e70900af4ab688decf6f53](https://gist.github.com/robertpainsi/b632364184e70900af4ab688decf6f53)

#### Conventional Commit Standard
All commit message should adhere to [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/) standards. We use this because of readability, and mainly because we use the feature to automatically bump version and generate CHANGELOG.md.

Conventional Commits format is as following:

```
<type>[optional scope]: <description>

[optional body]

[optional footer(s)]
```

Where `type` is the type of the commit, and `description` is the summary of the commit. You can add additional scope to further pinpoint where does the commit applying to.

Optional footer can be used to reference issues which this commit fixes.

---

Examples for a wrong commit message:
```
fix commit
```

```
code fix
```

```
idk what this does
```

examples for a right commit message:
```
fix: Fix incorrect data sent to Edit Menu

Edit CRUD service sent an incorrect data to the Edit Menu which crashes the menu.

Fixes #69: Edit Menu recieved incorrect data
BREAKING CHANGES: Data sent from Edit CRUD settings structure has changed
```

---

### Git Workflow

#### Adding Changes

If you want to add/fix our code, please create a new branch from `master`,  add/fix our code there, and create a pull request for your branch. 

The branch naming should adhere to this format:
```
<your name here>/<feature name here>
```

Why do we use this format? We use this because it's easy to organize the branches- in case when the branch number getting out of hands.

#### Create an Issue Ticket

If you want to create an Issue Ticket, please use the template linked here: 
[ISSUE_TEMPLATE.md](ISSUE_TEMPLATE.md)

Before creating issue ticket, please check if the issue checked all requirement below:
- [ ] Is it a bug
- [ ] Is it reproducable

#### Creating Pull Requests

If you want to create a pull request, your pull request commit should use this template linked below, and fill in the question.

[PR_TEMPLATE.md](PR_TEMPLATE.md)

## Disclaimer

By contributing to this repository, you agree to follow our standards which on violation will be met with a warning notice from us.
