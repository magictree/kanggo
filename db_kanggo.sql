-- --------------------------------------------------------
-- Host:                         
-- Server version:               10.1.32-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             11.1.0.6116
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for db_kanggo
DROP DATABASE IF EXISTS `db_kanggo`;
CREATE DATABASE IF NOT EXISTS `db_kanggo` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_bin */;
USE `db_kanggo`;

-- Dumping structure for table db_kanggo.list_cart
DROP TABLE IF EXISTS `list_cart`;
CREATE TABLE IF NOT EXISTS `list_cart` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` varchar(50) COLLATE utf8_bin NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `qty` int(3) NOT NULL,
  `amount` int(8) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_list_cart_product` (`product_id`),
  KEY `FK_list_cart_users` (`user_id`),
  KEY `Index 4` (`order_id`),
  CONSTRAINT `FK_list_cart_product` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_list_cart_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Dumping data for table db_kanggo.list_cart: ~2 rows (approximately)
DELETE FROM `list_cart`;
/*!40000 ALTER TABLE `list_cart` DISABLE KEYS */;
INSERT INTO `list_cart` (`id`, `order_id`, `product_id`, `user_id`, `qty`, `amount`, `status`, `created_at`, `updated_at`) VALUES
	(1, 'TRX/2021/06/1', 1, 19, 2, 30000000, 1, '2021-06-15 05:52:11', '2021-06-15 07:15:07'),
	(2, 'TRX/2021/06/1', 2, 19, 3, 15000000, 1, '2021-06-15 05:52:11', '2021-06-15 07:15:07');
/*!40000 ALTER TABLE `list_cart` ENABLE KEYS */;

-- Dumping structure for table db_kanggo.product
DROP TABLE IF EXISTS `product`;
CREATE TABLE IF NOT EXISTS `product` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item` varchar(50) COLLATE utf8_bin NOT NULL,
  `qty` int(3) NOT NULL,
  `price` int(8) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `item_product` (`item`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Dumping data for table db_kanggo.product: ~2 rows (approximately)
DELETE FROM `product`;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` (`id`, `item`, `qty`, `price`, `status`, `created_at`, `updated_at`) VALUES
	(1, 'Notebook Sharp 1', 100, 15000000, 1, '2021-06-15 02:32:00', '2021-06-15 02:32:00'),
	(2, 'Notebook Lenovo XL', 200, 5000000, 0, '2021-06-15 02:38:30', '2021-06-15 02:52:19');
/*!40000 ALTER TABLE `product` ENABLE KEYS */;

-- Dumping structure for table db_kanggo.transaction
DROP TABLE IF EXISTS `transaction`;
CREATE TABLE IF NOT EXISTS `transaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL,
  `order_id` varchar(50) COLLATE utf8_bin NOT NULL,
  `total_amount` int(10) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '0 : pending , 1: paid',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Dumping data for table db_kanggo.transaction: ~0 rows (approximately)
DELETE FROM `transaction`;
/*!40000 ALTER TABLE `transaction` DISABLE KEYS */;
INSERT INTO `transaction` (`id`, `user_id`, `order_id`, `total_amount`, `status`, `created_at`, `updated_at`) VALUES
	(2, 19, 'TRX/2021/06/1', 45000000, 1, '2021-06-15 07:15:07', '2021-06-15 07:15:07');
/*!40000 ALTER TABLE `transaction` ENABLE KEYS */;

-- Dumping structure for table db_kanggo.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fullname` varchar(30) COLLATE utf8_bin NOT NULL,
  `email` varchar(30) COLLATE utf8_bin NOT NULL,
  `password` text COLLATE utf8_bin NOT NULL,
  `contact` varchar(17) COLLATE utf8_bin DEFAULT NULL,
  `remember_token` text COLLATE utf8_bin,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `email_users` (`email`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Dumping data for table db_kanggo.users: ~1 rows (approximately)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `fullname`, `email`, `password`, `contact`, `remember_token`, `created_at`, `updated_at`) VALUES
	(19, 'agus triadji', 'agus.triadji@gmail.com', '$2b$10$jvH6vvJA/Po/.PfZbS33m.ALrqCnqrsYPEnYUlpjNAhAm6n7xtUQC', '+62 838 1234 8888', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTksImZ1bGxuYW1lIjoiYWd1cyB0cmlhZGppIiwiZW1haWwiOiJhZ3VzLnRyaWFkamlAZ21haWwuY29tIiwiY29udGFjdCI6Iis2MiA4MzggMTIzNCA4ODg4IiwiaWF0IjoxNjIzNzE2MTc5LCJleHAiOjE2MjM4MDI1Nzl9.i6CdWCs8uSXK5_wxSt9iNSYmRsbMwACRI0SePRt4d4M', '2021-06-14 19:04:19', '2021-06-15 07:16:19'),
	(20, 'Abigail', 'abigail@gmail.com', '$2b$10$5kxQQB9y9yKoxKGE8jciPO3IWhcbHZOcsmC8tgWZncWjHHbgvbP3y', '+62 838 1234 8888', NULL, '2021-06-15 07:20:16', '2021-06-15 07:20:16');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for procedure db_kanggo.cart_check_amount
DROP PROCEDURE IF EXISTS `cart_check_amount`;
DELIMITER //
CREATE PROCEDURE `cart_check_amount`(
	IN `_product` VARCHAR(30)
)
BEGIN
SET @stmt1 = CONCAT('SELECT a.price, a.id, a.qty, a.item from product a WHERE a.id IN(',_product,');');
PREPARE statements FROM @stmt1;
EXECUTE statements;

DEALLOCATE PREPARE statements;
END//
DELIMITER ;

-- Dumping structure for procedure db_kanggo.cart_create
DROP PROCEDURE IF EXISTS `cart_create`;
DELIMITER //
CREATE PROCEDURE `cart_create`(
	IN `_order` VARCHAR(50),
	IN `_product` INT(10),
	IN `_users` INT(10),
	IN `_qty` INT(3)
)
BEGIN
-- Create_order
SET @total = (SELECT (a.price * _qty) FROM product a WHERE a.id = _product);

-- SELECT _order AS 'a', _product AS 'b', _users AS 'c', _qty AS 'd', @total AS 'e' FROM DUAL;
INSERT INTO list_cart(`order_id`, `product_id`, `user_id`, `qty`,`amount`) 
VALUE(_order, _product, _users, _qty, @total);

SET @msg = 'Success.';
SET @codes = 200;

SELECT @msg AS 'message', @codes AS 'code' FROM DUAL;

END//
DELIMITER ;

-- Dumping structure for procedure db_kanggo.cart_order_id
DROP PROCEDURE IF EXISTS `cart_order_id`;
DELIMITER //
CREATE PROCEDURE `cart_order_id`(
	IN `_users` INT
)
BEGIN
SET @checks = (SELECT COUNT(a.order_id) FROM list_cart a WHERE a.user_id = _users);

if(@checks)then
	 
	SET @order_id = CONCAT('TRX/',DATE_FORMAT(NOW(), "%Y/%m/"),(@checks+1)); 
ELSE
	SET @order_id = CONCAT('TRX/',DATE_FORMAT(NOW(), "%Y/%m/"),1); 
END IF;

SELECT @order_id AS 'data', 'Success' AS 'message', 200 AS 'code' FROM DUAL;
END//
DELIMITER ;

-- Dumping structure for procedure db_kanggo.product_create
DROP PROCEDURE IF EXISTS `product_create`;
DELIMITER //
CREATE PROCEDURE `product_create`(
	IN `item` VARCHAR(30),
	IN `qty` INT(3),
	IN `price` INT(8)
)
BEGIN

-- CHECK item already exists
SET @checks = (SELECT a.item FROM product a WHERE a.item = item);

if(@checks) then
	
	SET @msg 		= CONCAT('Store failed, ',@checks,' already is exists.');
	SET @codes 	= 400;
	SELECT @msg AS 'message', @codes AS 'code' FROM DUAL;
ELSE
	
	SET @msg 		= 'Store Success.';
	SET @codes 	= 200;
	
	INSERT INTO product(`item`,`qty`,`price`) VALUE(item, qty, price);
	SELECT @msg AS 'message', @codes AS 'code' FROM DUAL;
END IF;

END//
DELIMITER ;

-- Dumping structure for procedure db_kanggo.product_delete
DROP PROCEDURE IF EXISTS `product_delete`;
DELIMITER //
CREATE PROCEDURE `product_delete`(
	IN `_id` INT(10)
)
BEGIN
-- CHECK id and item
SET @checks_id = (SELECT a.id FROM product a WHERE a.id = _id);
SET @checks_product_in_cart = (SELECT a.product_id FROM list_cart a WHERE a.product_id = _id);

if(@checks_id) then
	
	if(@checks_product_in_cart) then
		SET @msg 	= CONCAT('Delete failed, item ',@checks_id,' is being used on list cart ');
		SET @codes 	= 500;
		
		SELECT @msg AS 'message', @codes AS 'code' FROM DUAL;
	else
		SET @msg 	= 'Delete Success.';
		SET @codes 	= 200;
		
		UPDATE product SET `status` = 0 WHERE id = _id;
		SELECT @msg AS 'message', @codes AS 'code' FROM DUAL;
	END IF;
else
	
	SET @msg 	= CONCAT('Delete failed, items not found.');
	SET @codes 	= 500;
	SELECT @msg AS 'message', @codes AS 'code' FROM DUAL;
END IF;
END//
DELIMITER ;

-- Dumping structure for procedure db_kanggo.product_read
DROP PROCEDURE IF EXISTS `product_read`;
DELIMITER //
CREATE PROCEDURE `product_read`(
	IN `_id` INT(10),
	IN `_pages` INT(10),
	IN `_limits` INT(10)
)
BEGIN

if(_id > 0)then
	SET @msg = 'Read Success.';
	SET @codes = 200;
	SELECT @msg AS 'message', @codes AS 'code' FROM DUAL;
	SELECT a.id, a.item, a.qty, a.price FROM product a WHERE a.id = _id AND a.`status` = 1;
ELSE
	SET @msg = 'Read Success.';
	SET @codes = 200;
	SET _pages = _pages - 1;
	SELECT @msg AS 'message', @codes AS 'code' FROM DUAL;
	SELECT a.id, a.item, a.qty, a.price FROM product a WHERE a.`status` = 1 LIMIT _limits OFFSET _pages;
END IF;

END//
DELIMITER ;

-- Dumping structure for procedure db_kanggo.product_update
DROP PROCEDURE IF EXISTS `product_update`;
DELIMITER //
CREATE PROCEDURE `product_update`(
	IN `_id` INT(10),
	IN `_item` VARCHAR(30),
	IN `_qty` INT(3),
	IN `_price` INT(8)
)
BEGIN
-- CHECK id and item
SET @checks_id = (SELECT a.id FROM product a WHERE a.id = _id AND `status` = 1);
SET @checks_item = (SELECT a.id FROM product a WHERE a.item = _item);

if(@checks_id) then
	if(@checks_item != @checks_id) then
		
		SET @msg 	= CONCAT('Update failed, ',@checks_item,' already is exists.');
		SET @codes 	= 500;
		SELECT @msg AS 'message', @codes AS 'code' FROM DUAL;
	ELSE
		
		SET @msg 	= 'Update Success.';
		SET @codes 	= 200;
		
		UPDATE `product` SET item = _item, qty = _qty, price = _price WHERE id = _id AND `status` = 1;
		SELECT @msg AS 'message', @codes AS 'code' FROM DUAL;
	END IF;
else
	
	SET @msg 	= CONCAT('Update failed, items not found.');
	SET @codes 	= 500;
	SELECT @msg AS 'message', @codes AS 'code' FROM DUAL;
END IF;
END//
DELIMITER ;

-- Dumping structure for procedure db_kanggo.transaction_paid
DROP PROCEDURE IF EXISTS `transaction_paid`;
DELIMITER //
CREATE PROCEDURE `transaction_paid`(
	IN `_order` VARCHAR(50),
	IN `_users` INT
)
BEGIN

SET @total_amount = (SELECT SUM(a.amount) FROM list_cart a WHERE a.order_id = _order AND a.user_id = _users);

IF(@total_amount) then
	SET @msg = 'Success.';
	SET @codes = 200;
	
	INSERT INTO `transaction`(`user_id`,`order_id`,`total_amount`,`status`) VALUE(_users,_order, @total_amount, 1);
	UPDATE list_cart SET `status` = 1 WHERE order_id = _order AND user_id = _users;
	
	SELECT @msg AS 'message' , @codes AS 'code' FROM DUAL;
	
	SELECT 
	a.order_id, 
	a.product_id, 
	b.item, 
	a.qty, 
	(a.amount/a.qty) AS 'price_per_pcs',
	a.amount,  
	if(a.`status`=0,'Pending','Paid') AS 'status'
	from list_cart a 
	LEFT JOIN product b ON b.id = a.product_id
	WHERE a.user_id = _users AND a.order_id = _order
	ORDER BY a.order_id;
	
ELSE
	SET @msg = 'Transaction failed, order not found';
	SET @codes = 500;
	SELECT @msg AS 'message' , @codes AS 'code' FROM DUAL;
END IF;
END//
DELIMITER ;

-- Dumping structure for procedure db_kanggo.transaction_read
DROP PROCEDURE IF EXISTS `transaction_read`;
DELIMITER //
CREATE PROCEDURE `transaction_read`(
	IN `_order` VARCHAR(50),
	IN `_users` INT
)
BEGIN

SELECT 'View transaction' AS 'message', 200 AS 'code' FROM DUAL;
if(_order = '0')then

	SELECT 
	a.order_id, 
	a.product_id, 
	b.item, 
	a.qty,
	(a.amount/a.qty) AS 'price_per_pcs',
	a.amount,
	if(a.`status`=0,'Pending','Paid') AS 'status' 
	from list_cart a 
	LEFT JOIN product b ON b.id = a.product_id
	WHERE a.user_id = _users 
	ORDER BY a.order_id;

ELSE
	SELECT 
	a.order_id, 
	a.product_id, 
	b.item, 
	a.qty, 
	(a.amount/a.qty) AS 'price_per_pcs',
	a.amount,  
	if(a.`status`=0,'Pending','Paid') AS 'status'
	from list_cart a 
	LEFT JOIN product b ON b.id = a.product_id
	WHERE a.user_id = _users AND a.order_id = _order
	ORDER BY a.order_id;
END IF;

END//
DELIMITER ;

-- Dumping structure for procedure db_kanggo.users_auth_login
DROP PROCEDURE IF EXISTS `users_auth_login`;
DELIMITER //
CREATE PROCEDURE `users_auth_login`(
	IN `_email` VARCHAR(30),
	IN `_token` TEXT
)
BEGIN

-- CHECK
SET @checks_email = (SELECT a.id FROM users a WHERE a.email = _email);


IF(@checks_email)then
	SET @msg = 'Login Success';
	SET @codes = 200;
	UPDATE users SET `remember_token`=_token WHERE email = _email;
ELSE
	SET @msg = 'Email or password is not correctly.';
	SET @codes = 500;
END IF;

SELECT @msg AS 'message', @codes AS 'code' FROM DUAL;

END//
DELIMITER ;

-- Dumping structure for procedure db_kanggo.users_auth_logout
DROP PROCEDURE IF EXISTS `users_auth_logout`;
DELIMITER //
CREATE PROCEDURE `users_auth_logout`(
	IN `_email` VARCHAR(30)
)
BEGIN

SET @checks = (SELECT a.email FROM users a WHERE a.email = _email);

IF(@checks IS NULL)then
	SET @msg = 'Logout failed';
	SET @codes = 500;
ELSE
	SET @msg = 'Logout Success.';
	SET @codes = 200;
	UPDATE users SET remember_token = NULL WHERE email = _email;
	
END IF;

SELECT @msg AS 'message', @codes AS 'code' FROM DUAL; 

END//
DELIMITER ;

-- Dumping structure for procedure db_kanggo.users_check_email
DROP PROCEDURE IF EXISTS `users_check_email`;
DELIMITER //
CREATE PROCEDURE `users_check_email`(
	IN `_email` VARCHAR(30)
)
BEGIN

SET @msg = 'Read Success.';
SET @codes = 200;

SELECT @msg AS 'message', @codes AS 'code' FROM DUAL;
SELECT a.id, a.fullname, a.email, a.`password`,a.contact FROM users a WHERE a.email = _email;
	
END//
DELIMITER ;

-- Dumping structure for procedure db_kanggo.users_check_token
DROP PROCEDURE IF EXISTS `users_check_token`;
DELIMITER //
CREATE PROCEDURE `users_check_token`(
	IN `_email` VARCHAR(30),
	IN `_token` TEXT
)
BEGIN

SET @checks = (SELECT COUNT(a.id) FROM users a  WHERE a.email = _email AND a.remember_token = _token);

if(@checks)then
	SET @msg = 'Success.';
	SET @codes = 200;
ELSE
	SET @msg = 'failed';
	SET @codes = 500;
END IF;
SELECT @msg AS 'message', @codes AS 'code' FROM DUAL;
END//
DELIMITER ;

-- Dumping structure for procedure db_kanggo.users_create
DROP PROCEDURE IF EXISTS `users_create`;
DELIMITER //
CREATE PROCEDURE `users_create`(
	IN `_fullname` VARCHAR(30),
	IN `_email` VARCHAR(30),
	IN `_password` TEXT,
	IN `_contact` VARCHAR(18)
)
BEGIN

SET @checks = (SELECT a.email FROM users a WHERE a.email = _email);

if(@checks) then
	
	SET @msg 		= CONCAT('email already in exists');
	SET @codes 	= 400;
	SELECT @msg AS 'message', @codes AS 'code' FROM DUAL;
ELSE
	
	SET @msg 	= 'Store Success.';
	SET @codes 	= 200;
	
	if(_contact='0')then
		SET _contact = NULL;
	END IF;
	INSERT INTO users(`fullname`,`email`,`password`,`contact`) VALUE(_fullname, _email, _password, _contact);
	SELECT @msg AS 'message', @codes AS 'code' FROM DUAL;
END IF;

END//
DELIMITER ;

-- Dumping structure for procedure db_kanggo.users_read
DROP PROCEDURE IF EXISTS `users_read`;
DELIMITER //
CREATE PROCEDURE `users_read`(
	IN `_id` INT(10),
	IN `_pages` INT(10),
	IN `_limits` INT(10)
)
BEGIN

if(_id > 0)then
	SET @msg = 'Read Success.';
	SET @codes = 200;
	SELECT @msg AS 'message', @codes AS 'code' FROM DUAL;
	SELECT a.id, a.fullname, a.email, a.contact FROM users a WHERE a.id = _id;
ELSE
	SET @msg = 'Read Success.';
	SET @codes = 200;
	SET _pages = _pages - 1; 
	SELECT @msg AS 'message', @codes AS 'code' FROM DUAL;
	SELECT a.id, a.fullname, a.email, a.contact FROM users a LIMIT _limits OFFSET _pages;
END IF;


END//
DELIMITER ;

-- Dumping structure for procedure db_kanggo.users_update
DROP PROCEDURE IF EXISTS `users_update`;
DELIMITER //
CREATE PROCEDURE `users_update`(
	IN `_id` INT(10),
	IN `_fullname` VARCHAR(30),
	IN `_email` VARCHAR(30),
	IN `_contact` VARCHAR(30)
)
BEGIN

-- CHECK
SET @checks_id = (SELECT a.id FROM users a WHERE a.id = _id);
SET @checks_email = (SELECT a.id FROM users a WHERE a.email = _email);

if(@checks_id) then
	if(@checks_email) then
		if(@checks_email = _id) then
		
			SET @msg 	= 'Update Success';
			SET @codes 	= 200;
		else
			SET @msg 	= 'Update failed, Email already in used';
			SET @codes 	= 500;
		END IF;
	ELSE
		
		SET @msg 	= 'Update Success.';
		SET @codes 	= 200;
	END IF;
else
	
	SET @msg 	= CONCAT('Update failed, user not found.');
	SET @codes 	= 500;
END IF;

IF(@codes = 200)then
	UPDATE users SET fullname = _fullname, email = _email, contact = _contact WHERE id = _id;
END IF;

SELECT @msg AS 'message', @codes AS 'code' FROM DUAL;

END//
DELIMITER ;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
