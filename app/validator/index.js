/** @format */

module.exports = {
    //auth
    auth_login: require('./auth_login'),
    //user
    users_create: require('./users_create'),
    users_read: require('./users_read'),
    users_update: require('./users_update'),
    //product
    product_create: require('./product_create'),
    product_read: require('./product_read'),
    product_update: require('./product_update'),
    product_delete: require('./product_delete'),
    //cart
    cart_create: require('./cart_create'),
    cart_read: require('./cart_read'),
    //transaction
    transaction_paid: require('./transaction_paid'),
    transaction_read: require('./transaction_read')
};
