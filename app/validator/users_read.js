/** @format */

const Joi = require('@hapi/joi');

const schema = Joi.object().keys({
    id: Joi.number()
        .required()
        .allow(null),
    pages: Joi.number()
        .required()
        .allow(null),
    limits: Joi.number()
        .required()
        .allow(null)
});

let err = false;

module.exports = async (data, callback) => {
    try {
        let res = await schema.validateAsync(data);

        if (res.id) {
            if (res.pages || res.limits) {
                err = true;
            }
        } else {
            if (res.pages == null || res.limits == null) {
                err = true;
            }
        }

        return callback(err, res);
    } catch (error) {
        return callback(true, error.details);
    }
};
