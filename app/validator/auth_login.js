/** @format */

const Joi = require('@hapi/joi');

const schema = Joi.object().keys({
    email: Joi.string()
        .email()
        .required(),
    password: Joi.string()
        .min(8)
        .max(30)
        .required()
});

module.exports = async (data, callback) => {
    try {
        let res = await schema.validateAsync(data);
        return callback(null, res);
    } catch (error) {
        return callback(true, error.details);
    }
};
