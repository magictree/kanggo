/** @format */

const Joi = require('@hapi/joi');

const schema = Joi.object().keys({
    id: Joi.number().required(),
    item: Joi.string()
        .min(3)
        .max(30)
        .regex(/^[a-zA-Z1-9 \.\-\_\[\]]*$/)
        .required(),
    qty: Joi.number()
        .min(1)
        .max(9999)
        .required(),
    price: Joi.number()
        .min(3)
        .max(9999999999)
        .required()
});

module.exports = async (data, callback) => {
    try {
        let res = await schema.validateAsync(data);
        return callback(null, res);
    } catch (error) {
        return callback(true, error.details);
    }
};
