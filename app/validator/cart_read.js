/** @format */

const Joi = require('@hapi/joi');

const schema = Joi.object().keys({
    users: Joi.number()
        .required()
        .allow(null),
    order: Joi.string()
        .required()
        .allow(null)
});

module.exports = async (data, callback) => {
    try {
        let res = await schema.validateAsync(data);
        return callback(null, res);
    } catch (error) {
        return callback(true, error.details);
    }
};
