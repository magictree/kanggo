/** @format */

const Joi = require('@hapi/joi');

const schema = Joi.object().keys({
    cart: Joi.array().items(
        Joi.object().keys({
            product_id: Joi.number().required(),
            qty: Joi.number()
                .min(1)
                .max(999)
                .required()
        })
    )
});

module.exports = async (data, callback) => {
    try {
        let res = await schema.validateAsync(data);
        return callback(null, res);
    } catch (error) {
        return callback(true, error.details);
    }
};
