/** @format */

const Joi = require('@hapi/joi');

const schema = Joi.object().keys({
    id: Joi.number().required(),
    fullname: Joi.string()
        .regex(/^[a-zA-Z \.]{30}$/)
        .required(),
    email: Joi.string()
        .email()
        .required(),
    contact: Joi.string()
        .min(15)
        .max(17)
});

module.exports = async (data, callback) => {
    try {
        let res = await schema.validateAsync(data);
        return callback(null, res);
    } catch (error) {
        return callback(true, error.details);
    }
};
