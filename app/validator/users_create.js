/** @format */

const Joi = require('@hapi/joi');

const schema = Joi.object().keys({
    fullname: Joi.string()
        .min(3)
        .max(30)
        .regex(/^[a-zA-Z ]*$/)
        .required(),
    email: Joi.string()
        .email()
        .required(),
    password: Joi.string()
        .min(8)
        .max(30)
        .required(),
    contact: Joi.string()
        .min(15)
        .max(17)
        .allow(null)
});

module.exports = async (data, callback) => {
    try {
        let res = await schema.validateAsync(data);
        return callback(null, res);
    } catch (error) {
        return callback(true, error.details);
    }
};
