/** @format */

const ENV = process.env;

module.exports = {
    /**
     * service connection
     *
     * @param	{string}	NODE_PORT
     * @param	{string}	NODE_HOST
     */
    port: ENV.NODE_PORT || '8000',
    ip: ENV.NODE_HOST || '0.0.0.0',

    /**
     * mysql database connection
     *
     * @param	{string}	DB_HOST
     * @param	{string}	DB_PORT
     * @param	{string}	DB_USER
     * @param	{string}	DB_PASSWORD
     */
    mysql: {
        host: ENV.DB_HOST || '127.0.0.1',
        port: ENV.DB_PORT || '3306',
        user: ENV.DB_USER || 'root',
        password: ENV.DB_PASSWORD || '',
        database: 'db_kanggo'
    },

    procedure: {
        // AUTH
        users_check_email: 'users_check_email',
        users_auth_login: 'users_auth_login',
        users_auth_logout: 'users_auth_logout',
        users_check_token: 'users_check_token',
        // USERS
        users_create: 'users_create',
        users_read: 'users_read',
        users_update: 'users_update',
        //PRODUCT
        product_create: 'product_create',
        product_read: 'product_read',
        product_update: 'product_update',
        product_delete: 'product_delete',
        //CART / ORDER
        cart_create: 'cart_create',
        cart_order_id: 'cart_order_id',
        cart_check_amount: 'cart_check_amount',
        //TRANSACTION
        transaction_read: 'transaction_read',
        transaction_paid: 'transaction_paid'
    }
};
