/** @format */

const database = require('../connection/mysql');
const config = require('../config.js');
let response = {
    header: {
        message: null,
        status: null
    },
    data: null
};

module.exports = (data, callback) => {
    const { fullname, email, password, contact } = data;
    database.query(
        'CALL ' + config.procedure.users_create + '(?, ?, ?, ?)',
        [fullname, email, password, contact || '0'],
        (err, res) => {
            if (err) {
                response.header.message = err.sqlMessage;
                response.header.status = 500;
            } else {
                response.header.message = res[0][0].message;
                response.header.status = res[0][0].code;
            }
            return callback(response);
        }
    );
};
