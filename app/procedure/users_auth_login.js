/** @format */

const database = require('../connection/mysql');
const config = require('../config.js');

let response = {
    header: {
        message: null,
        status: null
    },
    data: null
};

module.exports = (data, callback) => {
    const { email, token } = data;
    database.query(
        'CALL ' + config.procedure.users_auth_login + '(?, ?)',
        [email, token],
        (err, res) => {
            if (err) {
                response.header.message = err.sqlMessage;
                response.header.status = 500;

                return callback(response);
            } else {
                response.header.message = res[0][0].message;
                response.header.status = res[0][0].code;
                response.data = {
                    fullname: data.fullname,
                    email: data.email,
                    token: data.token
                };
                return callback(response);
            }
        }
    );
};
