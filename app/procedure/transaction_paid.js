/** @format */

const async = require('async');
const database = require('../connection/mysql');
const config = require('../config.js');
let response = {
    header: {
        message: null,
        status: null
    },
    data: null
};

let order_id = null;

module.exports = (data, callback) => {
    database.query(
        'CALL ' + config.procedure.transaction_paid + '(?, ?)',
        [data.order || '0', data.users],
        (err, res) => {
            if (err) {
                response.header.message = err.sqlMessage;
                response.header.status = 500;
                return cb(response, null);
            } else {
                response.header.message = res[0][0].message;
                response.header.status = res[0][0].code;

                if (res[1].length > 0) {
                    // grouping
                    let group = [];
                    for (var i = 0; i < res[1].length; i++) {
                        if (group.length > 0) {
                            let idx = group.findIndex(str => {
                                return str.order_id === res[1][i].order_id;
                            });

                            if (idx != -1) {
                                group[idx].total_amount += parseInt(
                                    res[1][i].amount
                                );
                                group[idx].product.push({
                                    id: res[1][i].product_id,
                                    item: res[1][i].item,
                                    qty: res[1][i].qty,
                                    price: parseInt(res[1][i].price_per_pcs)
                                });
                            } else {
                                group.push({
                                    order_id: res[1][i].order_id,
                                    status: res[1][i].status,
                                    total_amount: parseInt(res[1][i].amount),
                                    product: [
                                        {
                                            id: res[1][i].product_id,
                                            item: res[1][i].item,
                                            qty: res[1][i].qty,
                                            price: parseInt(
                                                res[1][i].price_per_pcs
                                            )
                                        }
                                    ]
                                });
                            }
                        } else {
                            group.push({
                                order_id: res[1][i].order_id,
                                status: res[1][i].status,
                                total_amount: parseInt(res[1][i].amount),
                                product: [
                                    {
                                        id: res[1][i].product_id,
                                        item: res[1][i].item,
                                        qty: res[1][i].qty,
                                        price: parseInt(res[1][i].price_per_pcs)
                                    }
                                ]
                            });
                        }
                    }

                    response.data = group[0];
                } else {
                    response.data = null;
                }

                return callback(response);
            }
        }
    );
};
