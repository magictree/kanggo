/** @format */

const async = require('async');
const database = require('../connection/mysql');
const config = require('../config.js');
let response = {
    header: {
        message: null,
        status: null
    },
    data: null
};

let order_id = null;

module.exports = (data, callback) => {
    async.waterfall(
        [
            cb => {
                database.query(
                    'CALL ' + config.procedure.cart_order_id + '(?)',
                    [data.users],
                    (err, res) => {
                        if (err) {
                            response.header.message = err.sqlMessage;
                            response.header.status = 500;
                            return cb(response, null);
                        } else {
                            data.order_id = res[0][0].data;
                            cb(null, data);
                        }
                    }
                );
            }
        ],
        (err, val) => {
            if (err) return callback(err);

            async.eachSeries(
                val.cart,
                (entry, next) => {
                    async.setImmediate(() => {
                        database.query(
                            'CALL ' +
                                config.procedure.cart_create +
                                '(?, ?, ?, ?)',
                            [
                                val.order_id,
                                entry.product_id,
                                val.users,
                                entry.qty
                            ],
                            (err, res) => {
                                if (err) {
                                    response.header.message = err.sqlMessage;
                                    response.header.status = 500;
                                    return next(response);
                                }
                                next();
                            }
                        ); // end db 2
                    });
                },
                err => {
                    if (err) {
                        return callback(err);
                    } else {
                        response.header.message = 'Store cart success';
                        response.header.status = 200;
                        return callback(response);
                    }
                }
            ); // end series
        }
    ); // end waterfall
};
