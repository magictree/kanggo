/** @format */

const database = require('../connection/mysql');
const config = require('../config.js');

let response = {
    header: {
        message: null,
        status: null
    },
    data: null
};

module.exports = (data, callback) => {
    const { email, token } = data;
    database.query(
        'CALL ' + config.procedure.users_check_token + '(?, ?)',
        [email, token],
        (err, res) => {
            if (err) {
                response.header.message = err.sqlMessage;
                response.header.status = 500;
            } else {
                response.header.message = res[0][0].message;
                response.header.status = res[0][0].code;
            }

            return callback(response);
        }
    );
};
