/** @format */

const database = require('../connection/mysql');
const config = require('../config.js');
let response = {
    header: {
        message: null,
        status: null
    },
    data: null
};

module.exports = (data, callback) => {
    const { item, qty, price } = data;
    database.query(
        'CALL ' + config.procedure.product_create + '(?, ?, ?)',
        [item, qty, price],
        (err, res) => {
            if (err) {
                response.header.message = err.sqlMessage;
                response.header.status = 500;
                return callback(response, false);
            } else {
                return callback(false, res[0]);
            }
        }
    );
};
