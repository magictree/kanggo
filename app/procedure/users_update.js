/** @format */

const database = require('../connection/mysql');
const config = require('../config.js');

module.exports = (data, callback) => {
    const { id, fullname, email, password, contact } = data;
    database.query(
        'CALL ' + config.procedure.users_update + '(?, ?, ?, ?)',
        [id, fullname, email, password, contact],
        (err, res) => {
            if (err) return callback(true, err.sqlMessage);
            return callback(null, res);
        }
    );
};
