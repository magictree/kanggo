/** @format */

const database = require('../connection/mysql');
const config = require('../config.js');
const bcrypt = require('bcrypt');
const saltRounds = 10;
let response = {
    header: {
        message: null,
        status: null
    },
    data: null
};

module.exports = (data, callback) => {
    const { email } = data;
    database.query(
        'CALL ' + config.procedure.users_check_email + '(?)',
        [email],
        (err, res) => {
            if (err) {
                response.header.message = err.sqlMessage;
                response.header.status = 500;

                return callback(response);
            } else {
                bcrypt.compare(data.password, res[1][0].password, function(
                    err,
                    result
                ) {
                    if (err || !result) {
                        response.header.message =
                            'Email or Password is not correctly.';
                        response.header.status = 500;
                    } else {
                        if (!res[1]) {
                            response.header.message =
                                'Email or Password is not correctly.';
                            response.header.status = 500;
                        } else {
                            response.header.message = res[0][0].message;
                            response.header.status = res[0][0].code;
                            response.data = res[1][0];
                        }
                    }
                    return callback(response);
                });
            }
        }
    );
};
