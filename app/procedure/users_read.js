/** @format */

const database = require('../connection/mysql');
const config = require('../config.js');
let response = {
    header: {
        message: null,
        status: null
    },
    data: null
};

module.exports = (data, callback) => {
    let { id, pages, limits } = data;
    database.query(
        'CALL ' + config.procedure.users_read + '(?, ?, ?)',
        [id || 0, pages || 0, limits || 0],
        (err, res) => {
            if (err) {
                response.header.message = err.sqlMessage;
                response.header.status = 500;
            } else {
                response.header.message = res[0][0].message;
                response.header.status = res[0][0].code;

                response.data = res[1][0];
                if (!data.id) response.data = res[1];

                if (!response.data) {
                    response.header.message = 'Data not found';
                    response.header.status = res[0][0].code;
                    response.data = [];
                }
            }

            return callback(response);
        }
    );
};
