/** @format */

const database = require('../connection/mysql');
const config = require('../config.js');
let response = {
    header: {
        message: null,
        status: null
    },
    data: null
};

module.exports = (data, callback) => {
    const { id, pages, limits } = data;
    database.query(
        'CALL ' + config.procedure.product_read + '(?, ?, ?)',
        [id, pages, limits],
        (err, res) => {
            if (err) {
                response.header.message = err.sqlMessage;
                response.header.status = 500;
            } else {
                response.header.message = res[0][0].message;
                response.header.status = res[0][0].code;
                response.data = res[1][0];
                if (!data.id) response.data = res[1];
            }
            return callback(response);
        }
    );
};
