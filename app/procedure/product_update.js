/** @format */

const database = require('../connection/mysql');
const config = require('../config.js');
let response = {
    header: {
        message: null,
        status: null
    },
    data: null
};

module.exports = (data, callback) => {
    const { id, item, qty, price } = data;
    console.log(id, item, qty, price);
    database.query(
        'CALL ' + config.procedure.product_update + '(?, ?, ?, ?)',
        [id, item, qty, price],
        (err, res) => {
            if (err) {
                response.header.message = err.sqlMessage;
                response.header.status = 500;
            } else {
                response.header.message = res[0][0].message;
                response.header.status = res[0][0].code;
            }
            return callback(response);
        }
    );
};
