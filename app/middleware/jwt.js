/** @format */

const jwt = require('jsonwebtoken');
const procedure = require('./../procedure');
const secret = 'asdf1234*';
let res_data = {
    header: {
        message: null,
        status: null
    },
    data: null
};
module.exports = (req, res, next) => {
    let err = 'Token Unauthorized';
    let decoded = null;
    let dt = null;
    try {
        if (req.header('Authorization')) {
            dt = req.header('Authorization').split('Bearer ');
            decoded = jwt.verify(dt[1], secret); //Decoding JWT using keySecret

            if (decoded.email) {
                req.param.users = decoded;
            }
        }
    } catch (e) {
        res_data.header.status = 500;
        res_data.header.message = err;
        return res.status(500).json(res_data);
    }

    if (!decoded) {
        res_data.header.status = 500;
        res_data.header.message = err;
    } else {
        procedure.users_check_token(
            { token: dt[1], email: decoded.email },
            val => {
                if (val.header.status == 200) {
                    next();
                } else {
                    res_data.header.status = 500;
                    res_data.header.message = err;
                    return res.status(500).json(res_data);
                }
            }
        );
    }
};
