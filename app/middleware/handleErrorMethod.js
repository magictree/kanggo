/** @format */

module.exports = (req, res, next) => {
    /*
     * Handle Error page html
     */
    res.status(404);
    if (req.accepts('html')) {
        res.send('Sorry, your request cannot be found.');
        return;
    }
    res.type('txt').send('Not found');
};
