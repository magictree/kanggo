/** @format */

const express = require('express');
const app = express();
const router = express.Router();
const controller = require('../../controller');
const middle = require('../../middleware');

module.exports = app => {
    /**
     * /api/auth
     *
     * @method 	{POST}
     */
    app.post('/api/auth/login', async (req, res) => {
        controller.auth_login(req, message => {
            res.status(message.header.status).send(message);
        });
    });
    app.get('/api/auth/logout', middle.jwt, async (req, res) => {
        controller.auth_logout(req, message => {
            res.status(message.header.status).send(message);
        });
    });
    /**
     * /api/users
     *
     * @method 	{POST}
     */
    app.post('/api/users/read', middle.jwt, async (req, res) => {
        controller.users_read(req, message => {
            res.status(message.header.status).send(message);
        });
    });
    app.post('/api/users/create', middle.jwt, async (req, res) => {
        controller.users_create(req, message => {
            res.status(message.header.status).send(message);
        });
    });
    app.post('/api/users/update', middle.jwt, async (req, res) => {
        controller.users_create(req, message => {
            res.status(message.header.status).send(message);
        });
    });

    /**
     * /api/product
     *
     * @method 	{POST}
     */
    app.post('/api/product/read', middle.jwt, async (req, res) => {
        controller.product_read(req, message => {
            res.status(message.header.status).send(message);
        });
    });
    app.post('/api/product/create', middle.jwt, async (req, res) => {
        controller.product_create(req, message => {
            res.status(message.header.status).send(message);
        });
    });
    app.post('/api/product/update', middle.jwt, async (req, res) => {
        controller.product_update(req, message => {
            res.status(message.header.status).send(message);
        });
    });
    app.post('/api/product/delete', middle.jwt, async (req, res) => {
        controller.product_delete(req, message => {
            res.status(message.header.status).send(message);
        });
    });

    /**
     * /api/cart
     *
     * @method 	{POST}
     */
    app.post('/api/cart/read', middle.jwt, async (req, res) => {
        controller.cart_read(req, message => {
            res.status(message.header.status).send(message);
        });
    });
    app.post('/api/cart/create', middle.jwt, async (req, res) => {
        controller.cart_create(req, message => {
            res.status(message.header.status).send(message);
        });
    });

    /**
     * /api/transaction
     *
     * @method 	{POST}
     */
    app.post('/api/transaction/read', middle.jwt, async (req, res) => {
        controller.transaction_read(req, message => {
            res.status(message.header.status).send(message);
        });
    });
    app.post('/api/transaction/paid', middle.jwt, async (req, res) => {
        controller.transaction_paid(req, message => {
            res.status(message.header.status).send(message);
        });
    });
};
