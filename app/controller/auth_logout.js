/** @format */

const async = require('async');
const procedure = require('../procedure');
const jwt = require('jsonwebtoken');
const secret = 'asdf1234*';
const action = 'read';
const form = 'Auth';
let response = require('./../library/response');

module.exports = (req, callback) => {
    async.waterfall(
        [
            next => {
                /**
                 * store procedure
                 */
                procedure.users_auth_logout(req.param.users, val => {
                    return next(val);
                });
            }
        ],
        (err, results) => {
            if (err) results = err;

            callback(results);
        }
    );
};
