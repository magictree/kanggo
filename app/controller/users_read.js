/** @format */

const async = require('async');
const validator = require('../validator');
const procedure = require('../procedure');
const bcrypt = require('bcrypt');
const saltRounds = 10;
const action = 'read';
const form = 'Users';
let response = require('./../library/response');

module.exports = (req, callback) => {
    async.waterfall(
        [
            next => {
                /**
                 * Validator check params from client
                 * @callback Requester~requestCallback
                 * @param {string} req
                 * @param {string} err		The callback that handles the response error.
                 * @param {string} value	The callback that handles the response succes.
                 */

                validator.users_read(req.body, (err, value) => {
                    if (err) {
                        let res = response(action, form, null);
                        res.header.message = 'Validation failed';
                        return next(res, null);
                    } else {
                        next(null, value);
                    }
                });
            },
            (body, next) => {
                /**
                 * Store Procedure
                 */
                procedure.users_read(body, val => {
                    next(null, val);
                });
            }
        ],
        (err, results) => {
            if (err) results = err;

            return callback(results);
        }
    );
};
