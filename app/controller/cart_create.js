/** @format */

const async = require('async');
const validator = require('../validator');
const procedure = require('../procedure');
const action = 'create';
const form = 'Cart';
let response = require('./../library/response');

module.exports = (req, callback) => {
    async.waterfall(
        [
            next => {
                /**
                 * Validator check params from client
                 * @callback Requester~requestCallback
                 * @param {string} req
                 * @param {string} err		The callback that handles the response error.
                 * @param {string} value	The callback that handles the response succes.
                 */

                validator.cart_create(req.body, (err, value) => {
                    if (err) {
                        let res = response(action, form, null);
                        res.header.message = 'Validation failed';
                        return next(res, null);
                    } else {
                        next(null, value);
                    }
                });
            },
            (body, next) => {
                /**
                 * Store Procedure
                 */
                body.users = req.param.users.id;
                body.email = req.param.users.email;

                procedure.cart_create(body, val => {
                    console.log(val);
                    next(null, val);
                });
            }
        ],
        (err, results) => {
            if (err) results = err;

            callback(results);
        }
    );
};
