/** @format */

const async = require('async');
const validator = require('../validator');
const procedure = require('../procedure');
const action = 'update';
const form = 'Product';
let response = require('./../library/response');

module.exports = (req, callback) => {
    async.waterfall(
        [
            next => {
                /**
                 * Validator check params from client
                 * @callback Requester~requestCallback
                 * @param {string} req
                 * @param {string} err		The callback that handles the response error.
                 * @param {string} value	The callback that handles the response succes.
                 */

                validator.product_update(req.body, (err, value) => {
                    if (err) {
                        let res = response(action, form, null);
                        res.header.message = 'Validation failed';
                        return next(res, null);
                    } else {
                        next(null, value);
                    }
                });
            },
            (body, next) => {
                /**
                 * setup password
                 */
                bcrypt.hash(body.password, saltRounds, function(err, hash) {
                    body.password = hash;
                    next(null, body);
                });
            },
            (body, next) => {
                /**
                 * Store Procedure
                 */
                procedure.product_update(body, val => {
                    next(val);
                });
            }
        ],
        (err, results) => {
            if (err) results = err;

            callback(results);
        }
    );
};
