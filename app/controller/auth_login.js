/** @format */

const async = require('async');
const validator = require('../validator');
const procedure = require('../procedure');
const jwt = require('jsonwebtoken');
const secret = 'asdf1234*';
const action = 'read';
const form = 'Auth';
let response = require('./../library/response');

module.exports = (req, callback) => {
    async.waterfall(
        [
            next => {
                /**
                 * Validator check params from client
                 * @callback Requester~requestCallback
                 * @param {string} req
                 * @param {string} err		The callback that handles the response error.
                 * @param {string} value	The callback that handles the response succes.
                 */

                validator.auth_login(req.body, (err, value) => {
                    if (err) {
                        let res = response(action, form, null);
                        res.header.message = 'Validation failed';
                        return next(res, null);
                    } else {
                        next(null, value);
                    }
                });
            },
            (body, next) => {
                /**
                 * check Procedure
                 */
                procedure.users_check_email(body, val => {
                    if (val.header.status == 500) {
                        return next(val);
                    } else {
                        let data = {
                            id: val.data.id,
                            fullname: val.data.fullname,
                            email: val.data.email,
                            contact: val.data.contact
                        };
                        let token = jwt.sign(data, secret, {
                            expiresIn: '1d'
                        });
                        data.token = token;
                        next(null, data);
                    }
                });
            },
            (body, next) => {
                /**
                 * store procedure
                 */
                procedure.users_auth_login(body, val => {
                    return next(val);
                });
            }
        ],
        (err, results) => {
            if (err) results = err;

            callback(results);
        }
    );
};
