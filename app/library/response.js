/** @format */

/**
 *
 *
 * @param {string} action Request type
 * @param {string} form Page type
 * @param {Object} data Response data
 * @returns {Object} response
 */
module.exports = (action, form, data) => {
    let msg = null;
    const responseData = {
        header: {
            message: data,
            status: 500
        },
        data: null
    };

    if (form) {
        // jika form is null maka dianggap error

        if (action === 'store') {
            // STORE
            msg = `Store ${form} Successfully`;
        } else if (action === 'update') {
            // UPDATE
            msg = `Update ${form} Successfully`;
        } else if (action === 'delete') {
            // DELETE
            msg = `Delete ${form} Successfully`;
        } else {
            // READ
            msg = `Get data ${form} Successfully`;
        }

        responseData.header.status = 200;
        responseData.header.message = msg;
        responseData.data = data;
    }

    return responseData;
};
