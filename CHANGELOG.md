# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [0.3.0](https://gitlab.com/hrms-improvement/crud-template/compare/v0.2.0...v0.3.0) (2020-04-08)


### ⚠ BREAKING CHANGES

* **app:** Almost everything changes because of rewriting the code.

### Features

* **app:** rewrite (almost) all of the code ([53767d7](https://gitlab.com/hrms-improvement/crud-template/commit/53767d7a5d5166189aef8426bf9b69046dc2f334)), closes [#1](https://gitlab.com/hrms-improvement/crud-template/issues/1) [#2](https://gitlab.com/hrms-improvement/crud-template/issues/2) [#3](https://gitlab.com/hrms-improvement/crud-template/issues/3) [#4](https://gitlab.com/hrms-improvement/crud-template/issues/4) [#5](https://gitlab.com/hrms-improvement/crud-template/issues/5) [#6](https://gitlab.com/hrms-improvement/crud-template/issues/6) [#7](https://gitlab.com/hrms-improvement/crud-template/issues/7) [#8](https://gitlab.com/hrms-improvement/crud-template/issues/8) [#9](https://gitlab.com/hrms-improvement/crud-template/issues/9)

## [0.2.0](https://gitlab.com/hrms-improvement/crud-template/compare/v0.1.1...v0.2.0) (2020-03-31)


### ⚠ BREAKING CHANGES

* Response structure changed

### Features

* **App:** Add script to run app and format files ([ce5a96a](https://gitlab.com/hrms-improvement/crud-template/commit/ce5a96a239ef8990770c87d30dc3f8837fba6a3c))
* Add authentication ([7021c3f](https://gitlab.com/hrms-improvement/crud-template/commit/7021c3f6509c4d61903e2ec222edbb8015a081bb))
* Add README, CONTRIBUTING, and ticket or PR template ([04357d1](https://gitlab.com/hrms-improvement/crud-template/commit/04357d1c88a377c81969175727512e4ca874da06))
* Add ReadOffset API ([a6cc630](https://gitlab.com/hrms-improvement/crud-template/commit/a6cc63088f325fef6de11e458a270bc996307609))


### Bug Fixes

* **Controller:** Add more edge case for procedure results ([5d32086](https://gitlab.com/hrms-improvement/crud-template/commit/5d3208652d9aff4469b785887f218d0ee94feff5))
* **Controller:** Fix DELETE API error on used ID ([2a4d867](https://gitlab.com/hrms-improvement/crud-template/commit/2a4d86745b7d41665fcf98d98dd10c12bce0d55c))
* Change response structure to existing one ([669864c](https://gitlab.com/hrms-improvement/crud-template/commit/669864cc33f00d43c3b6d868e1181cb4f3eb0c75))
* Fix ESLint not linting properly ([81a67cf](https://gitlab.com/hrms-improvement/crud-template/commit/81a67cf65ac2d37e009243c092c52789894b2558))
* Minor fix for Update Controller file ([eb9e06e](https://gitlab.com/hrms-improvement/crud-template/commit/eb9e06ef2a7826787a0218cac3c8881629ef99f8))
* Move dependencies for development purposes to devDependencies ([0d194b4](https://gitlab.com/hrms-improvement/crud-template/commit/0d194b47b96795c2cdeeae0eb6a2631f548120f5))
* Remove unused configs and change port ([77dcbfd](https://gitlab.com/hrms-improvement/crud-template/commit/77dcbfdf7523039123c021f63b7e9127def4ac1e))
* Remove unused packages ([cc08d96](https://gitlab.com/hrms-improvement/crud-template/commit/cc08d965781c0e0d5f37689b34b7cf37ed529f6c))

### 0.1.1 (2020-03-11)


### Features

* Add default mySQL connection to HRMS DB and default config ([5f53a77](https://gitlab.com/hrms-improvement/crud-template/commit/5f53a770d71128761523a9c4c9140a7b89cd61ae))


### Bug Fixes

* Add missing dependencies ([302028c](https://gitlab.com/hrms-improvement/crud-template/commit/302028c675eac8d46fc8a662b6f8aed6675255c7))
* Add missing routes file ([0de310a](https://gitlab.com/hrms-improvement/crud-template/commit/0de310a7f90c2c9d3a32469fcd8de4ba68cd1e87))
* Change code formatting ([d8f0581](https://gitlab.com/hrms-improvement/crud-template/commit/d8f0581cd670c8c639976c1835cf3f059937400e))
* Fix indexGenerator and remove a directory from scan list ([4082559](https://gitlab.com/hrms-improvement/crud-template/commit/4082559548ba6d2981066fbfdb3b7c6a5046fa3b))
* Merge routes for the same route path but different methods ([cbfc17d](https://gitlab.com/hrms-improvement/crud-template/commit/cbfc17d1e4fac3baf3baedf4408bcfece4a200d3))
* Remove socket.io initalizer ([4e81a87](https://gitlab.com/hrms-improvement/crud-template/commit/4e81a87f68f1c00d4965624b9a09eea3f608ed30))
* Standardize Procedure codes ([1fff7b1](https://gitlab.com/hrms-improvement/crud-template/commit/1fff7b10be3667052cc9ad502cf009385f9b49cc))
* Updating JOI validation to a new one ([18d525e](https://gitlab.com/hrms-improvement/crud-template/commit/18d525ef20ba4334c30429d609aa6ac48d754782))
* Various fixes on controller section ([a8b0b78](https://gitlab.com/hrms-improvement/crud-template/commit/a8b0b78ce9d280c2174d2f389866c8db1ee2d609))
