<!-- @format -->

# README

## Table of contents

-   [What is this?](#what-is-this-)
-   [How to run this?](#how-to-run-this-)
-   [Document with postman](#document-with-postman) **READ THIS FIRST**
-   [How to contribute on this repository](CONTRIBUTING.md) - **DEVELOPER READ THIS FIRST**

## What is this

This program is a simple CRUD app for Kanggo submodules.

## How to run this

First, you need to install the requirements:

-   NodeJS v14
-   10.1.32-MariaDB (Support mysql 5.6)

    At least version 12 or above. If unsure, use the latest LTS version.

And that's it! For running it, just go to the app root directory and run this command

```bash
npm install;
node serve.js;
```

It will install all the package required for running it and then run the app.

## Document with postman

Please check postman collaction dir\test\POSTMAN

## How to contribute on this repository

Read [CONTRIBUTING.md](CONTRIBUTING.md)
